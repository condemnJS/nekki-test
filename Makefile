DC=docker-compose -f docker-compose.yaml

.ONESHELL:

TAB=echo "\t"

help: ## хэлпер
	@$(TAB) make up - поднять контейнеры
	@$(TAB) make down - вырубить контейнеры
	@$(TAB) make build-up - перебилдить контейнеры

up: ## поднять контейнеры
	${DC} up -d

down: ## вырубить контейнеры
	${DC} down

build-up: ## перебилдить контейнеры
	${DC} up --build --force-recreate -d

ps: ## получить информацию о контейнерах
	${DC} ps

php-cli:
	${DC} exec -it app bash