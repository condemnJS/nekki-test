<?php

declare(strict_types=1);

namespace App\Domain;

use App\Domain\Enums\TariffPlanType;
use App\Domain\Exceptions\DomainException;

class TariffPlan
{
    /** @var int */
    private int $id;

    /** @var TariffPlanType */
    private TariffPlanType $type;

    /** @var string */
    private string $name;

    /** @var int */
    private int $price;

    /** @var string */
    private string $description;

    /** @var bool */
    private bool $isActive;

    /**
     * @param string $type
     * @param string $name
     * @param int $price
     * @param string $description
     * @param bool $isActive
     *
     * @throws DomainException
     */
    public function __construct(string $type, string $name, int $price, string $description, bool $isActive)
    {
        $this->setType($type);
        $this->name = $name;
        $this->price = $price;
        $this->setDescription($description);
        $this->isActive = $isActive;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return TariffPlanType
     */
    public function getType(): TariffPlanType
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param string $type
     * @throws DomainException
     */
    public function setType(string $type): void
    {
        $chooseType = match ($type) {
            'free' => TariffPlanType::FREE,
            'pro' => TariffPlanType::PRO,
            'business' => TariffPlanType::BUSINESS,
            default => throw new DomainException('Неизвестный тип тарифного плана'),
        };

        $this->type = $chooseType;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getType()->value,
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'price' => $this->getPrice(),
        ];
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $description = preg_replace('#<img[^>]*\s*>#i', '', $description);
        $description = preg_replace(
            "#(https?|ftp)://\S+[^\s.,> )\];'\"!?]#",
            '<a href="\\0">\\0</a>',
            $description
        );

        $this->description = $description;
    }
}