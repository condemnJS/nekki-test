<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\DTO\CreateTariffPlanDTO;
use App\Application\Repository\TariffRepositoryInterface;
use App\Domain\Exceptions\DomainException;
use App\Domain\TariffPlan;

final class AddTariffPlanUseCase
{
    /**
     * @param TariffRepositoryInterface $tariffRepository
     */
    public function __construct(private readonly TariffRepositoryInterface $tariffRepository)
    {
    }

    /**
     * @param CreateTariffPlanDTO $tariffPlanDTO
     * @return TariffPlan
     *
     * @throws DomainException
     */
    public function __invoke(CreateTariffPlanDTO $tariffPlanDTO): TariffPlan
    {
        $tariffPlan = new TariffPlan(
            $tariffPlanDTO->getType(),
            $tariffPlanDTO->getName(),
            $tariffPlanDTO->getPrice(),
            $tariffPlanDTO->getDescription(),
            $tariffPlanDTO->isActive(),
        );

        $this->tariffRepository->save($tariffPlan);

        return $tariffPlan;
    }
}