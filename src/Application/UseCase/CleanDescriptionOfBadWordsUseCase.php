<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\Repository\BadWordsRepositoryInterface;

final class CleanDescriptionOfBadWordsUseCase
{
    /**
     * @param BadWordsRepositoryInterface $badWordsRepository
     */
    public function __construct(private readonly BadWordsRepositoryInterface $badWordsRepository)
    {
    }

    /**
     * @param string $description
     * 
     * @return string
     */
    public function __invoke(string $description): string
    {
        $badWords = $this->badWordsRepository->resolve();
        foreach ($badWords as $badWord) {
            $description = preg_replace("#$badWord#ui", "...", $description);
        }
        
        return $description;
    }
}