<?php

declare(strict_types=1);

namespace App\Application\DTO;

use App\Application\Support\ArgumentResolver\DtoResolvedInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreateTariffPlanDTO implements DtoResolvedInterface
{
    #[NotBlank()]
    private string $type;

    /**
     * @var string
     * @Assert\NotBlank
     */
    private string $name;

    /**
     * @var int
     * @Assert\NotBlank
     */
    private int $price;

    /**
     * @var string
     * @Assert\NotBlank
     */
    private string $description;

    /**
     * @var bool
     * @Assert\NotBlank
     */
    private bool $isActive;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }
}