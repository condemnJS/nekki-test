<?php

declare(strict_types=1);

namespace App\Application\Repository;

use App\Domain\TariffPlan;

interface TariffRepositoryInterface
{
    /**
     * @param TariffPlan $tariffPlan
     *
     * @return void
     */
    public function save(TariffPlan $tariffPlan): void;
}