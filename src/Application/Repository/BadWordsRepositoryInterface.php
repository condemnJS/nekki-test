<?php

declare(strict_types=1);

namespace App\Application\Repository;

interface BadWordsRepositoryInterface
{
    /**
     * @return array
     */
    public function resolve(): array;
}