<?php

namespace App;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Serializer\SerializerInterface;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function process(ContainerBuilder $container)
    {
        $container->setAlias(LoggerInterface::class, 'monolog.logger')->setPublic(true);
        $container->setAlias(SerializerInterface::class, 'serializer')->setPublic(true);
    }
}
