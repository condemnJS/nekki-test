<?php

declare(strict_types=1);

namespace App\Infrastructure\Events;

use App\Domain\TariffPlan;
use Symfony\Contracts\EventDispatcher\Event;

class TariffPlanCreatedEvent extends Event
{
    public const NAME = 'tariff.plan.created';

    /**
     * @param TariffPlan $tariffPlan
     */
    public function __construct(private readonly TariffPlan $tariffPlan)
    {
    }

    /**
     * @return TariffPlan
     */
    public function getTariffPlan(): TariffPlan
    {
        return $this->tariffPlan;
    }
}