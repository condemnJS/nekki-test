<?php

declare(strict_types=1);

namespace App\Infrastructure\Adapters\Support\ArgumentResolver;

use App\Application\Support\ArgumentResolver\DtoResolvedInterface;
use App\Infrastructure\Exceptions\ValidationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DtoValueResolver implements ValueResolverInterface
{
    /**
     * @param DenormalizerInterface $denormalizer
     * @param ValidatorInterface $validator
     */
    public function __construct(
        private readonly DenormalizerInterface $denormalizer,
        private readonly ValidatorInterface $validator
    ) {
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $argument->getType()
            && in_array(DtoResolvedInterface::class, class_implements($argument->getType()));
    }

    /**
     * @throws ExceptionInterface
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        // Проверяем имплеминтирует ли наш класс, интерфейс DtoResolvedInterface
        if (!$this->supports($request, $argument)) {
            throw new BadRequestHttpException();
        }

        try {
            // Парсим json to object
            $dto = $this->denormalizer->denormalize($request->toArray(), $argument->getType());
        } catch (NotNormalizableValueException) {
            throw new BadRequestHttpException();
        }

        // Проверяем валидацию
        $violations = $this->validator->validate($dto);
        if ($violations->count() > 0) {
            throw new ValidationException($violations);
        }

        yield $dto;
    }
}
