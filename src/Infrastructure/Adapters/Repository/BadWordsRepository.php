<?php

declare(strict_types=1);

namespace App\Infrastructure\Adapters\Repository;

use App\Application\Repository\BadWordsRepositoryInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class BadWordsRepository implements BadWordsRepositoryInterface
{
    /** @var string */
    private string $projectPath;

    /**
     * @param KernelInterface $kernel
     */
    public function __construct(private readonly KernelInterface $kernel)
    {
        $this->projectPath = $this->kernel->getProjectDir();
    }

    /**
     * @return array
     */
    public function resolve(): array
    {
        $fileStr = file_get_contents($this->projectPath . "/bad.json");
        if (!$fileStr) {
            return [];
        }

        return json_decode($fileStr);
    }
}