<?php

declare(strict_types=1);

namespace App\Infrastructure\Adapters\Repository;

use App\Application\Repository\TariffRepositoryInterface;
use App\Domain\TariffPlan;
use Doctrine\ORM\EntityManagerInterface;

class TariffPlanRepository implements TariffRepositoryInterface
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function save(TariffPlan $tariffPlan): void
    {
        $this->entityManager->persist($tariffPlan);
        $this->entityManager->flush();
    }
}