<?php

declare(strict_types=1);

namespace App\Infrastructure\Actions;

use App\Application\DTO\CreateTariffPlanDTO;
use App\Application\UseCase\AddTariffPlanUseCase;
use App\Application\UseCase\CleanDescriptionOfBadWordsUseCase;
use App\Domain\Exceptions\DomainException;
use App\Infrastructure\Events\TariffPlanCreatedEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

class CreateTariffAction
{
    /**
     * @param AddTariffPlanUseCase $addTariffUseCase
     * @param CleanDescriptionOfBadWordsUseCase $cleanDescriptionOfBadWordsUseCase
     * @param EventDispatcherInterface $eventDispatcher
     * @param LoggerInterface $logger
     */
    public function __construct(
        private readonly AddTariffPlanUseCase $addTariffUseCase,
        private readonly CleanDescriptionOfBadWordsUseCase $cleanDescriptionOfBadWordsUseCase,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly LoggerInterface $logger,
    )
    {
    }

    /**
     * @throws DomainException
     */
    #[Route('/tariff', name: 'create_tariff_plan', methods: ['POST'])]
    public function __invoke(CreateTariffPlanDTO $tariffDTO): JsonResponse
    {
        // Удаляем маты из описания
        $cleanDescription = $this->cleanDescriptionOfBadWordsUseCase;
        $tariffDTO->setDescription($cleanDescription($tariffDTO->getDescription()));

        // Создаем тариф
        $createTariffPlan = $this->addTariffUseCase;
        $tariffPlan = $createTariffPlan($tariffDTO);

        // Логируем
        $this->logger->info(
            'Tariff plan was created by number: ' . $tariffPlan->getId(),
            [
                'entity' => $tariffPlan->toArray(),
                'category' => 'TariffPlan',
            ]
        );

        // Вызываем событие, которое отправить письму админу
        $this->eventDispatcher->dispatch(new TariffPlanCreatedEvent($tariffPlan), TariffPlanCreatedEvent::NAME);

        return new JsonResponse(['message' => 'Тарифный план успешно создан!'], 201);
    }
}