<?php

declare(strict_types=1);

namespace App\Infrastructure\Subscribers;

use App\Domain\Exceptions\DomainException;
use App\Infrastructure\Exceptions\ValidationException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

class ExceptionKernelSubscriber implements EventSubscriberInterface
{
    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(private readonly SerializerInterface $serializer)
    {
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    /**
     * @param ExceptionEvent $event
     *
     * @return void
     * @throws Throwable
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $response = $this->getResponse($event);

        $response->setEncodingOptions(JsonResponse::DEFAULT_ENCODING_OPTIONS | JSON_UNESCAPED_UNICODE);
        $event->setResponse($response);
    }

    /**
     * @param ExceptionEvent $event
     *
     * @return JsonResponse
     * @throws Throwable
     */
    private function getResponse(ExceptionEvent $event): JsonResponse
    {
        $ex = $event->getThrowable();
        if ($ex instanceof ValidationException) {
            return new JsonResponse(['errors' => $ex->getValidationErrors()], 400);
        }
        // сюда также можно добавлять различные другие обработчики исключений

        if ($ex instanceof DomainException) {
            return new JsonResponse(['message' => $ex->getMessage()], 422);
        }

        throw $ex;
    }

}