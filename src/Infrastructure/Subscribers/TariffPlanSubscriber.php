<?php

declare(strict_types=1);

namespace App\Infrastructure\Subscribers;

use App\Infrastructure\Events\TariffPlanCreatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class TariffPlanSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly MailerInterface $mailer)
    {
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            TariffPlanCreatedEvent::NAME => 'onTariffPlanCreation'
        ];
    }

    /**
     * @param TariffPlanCreatedEvent $tariffPlanCreatedEvent
     *
     * @return void
     * @throws TransportExceptionInterface
     */
    public function onTariffPlanCreation(TariffPlanCreatedEvent $tariffPlanCreatedEvent): void
    {
        $tariffPlan = $tariffPlanCreatedEvent->getTariffPlan();

        $email = (new Email())
            ->from($_ENV['EMAIL_SENDER'])
            ->to($_ENV['ADMIN_EMAIL'])
            ->subject('Tariff plan by number: ' . $tariffPlan->getId() . ' was created')
            ->html("
                <p>Type: {$tariffPlan->getType()->value}</p>
                <p>Name: {$tariffPlan->getName()}</p>
                <p>Price: {$tariffPlan->getPrice()}</p>
                <p>Description: {$tariffPlan->getDescription()}</p>
            ");

        $this->mailer->send($email);
    }
}