<?php

declare(strict_types=1);

namespace App\Infrastructure\Exceptions;

use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends InvalidArgumentException
{
    /** @var ConstraintViolationListInterface */
    private ConstraintViolationListInterface $violationList;

    /**
     * @param ConstraintViolationListInterface $constraintViolationList
     * @param string|null $message
     */
    public function __construct(
        private readonly ConstraintViolationListInterface $constraintViolationList,
        ?string $message = null
    ) {
        $this->violationList = $constraintViolationList;

        $parentMessage = is_null($message) ? 'Ошибка валидации' : $message;
        $parentCode = is_null($message) ? Response::HTTP_BAD_REQUEST : Response::HTTP_INTERNAL_SERVER_ERROR;
        parent::__construct(
            $parentMessage,
            $parentCode
        );
    }

    /**
     * @return array
     */
    public function getValidationErrors(): array
    {
        $errors = [];

        foreach ($this->violationList as $violation) {
            $errors[$violation->getPropertyPath()] = $violation->getMessage();
        }

        return $errors;
    }
}